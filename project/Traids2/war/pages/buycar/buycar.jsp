<!DOCTYPE html>
<%@page import="com.triads.db.CarHanler"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.triads.model.Car"%>

<html lang="en">
  <head>
    <title>Buy A Car</title>  
       
    <!-- Custom styles for this template -->
    <link href="/pages/buycar/buycar.css" rel="stylesheet">   
  </head>
  <body>
  	<h1>Buy a car</h1>
	<ul id="items"> 
  	<%  
	CarHanler carHandler = new CarHanler();
    ArrayList<Car> cars = carHandler.AllCars();	
	
	for (Car car :cars) {
	  String regno = (String) car.getRegno();
	  String brand = (String) car.getBrand();
	  Float price = (Float) car.getPrice();		
	  pageContext.setAttribute("regno", regno);
	  pageContext.setAttribute("brand", brand);
	  pageContext.setAttribute("price", price);
	 
  	%>
  	<li class="item round">
<h3>RegNO:${fn:escapeXml(regno)} </h3>
<div class="imgbox">
<a href="#"><img src="/images/car1.jpg" class="tales" alt="" width="100" height="75"></a>
</div>
<div class="boxtext">
	<div class="boxintxt">Location : Battaramulla</div>
	<div class="boxintxt">Price :${fn:escapeXml(price)}</div>
	<div class="boxintxt">brand : ${fn:escapeXml(brand)}</div>
	<div class="boxintxt">Ad Date : 2014-03-08</div>
<div class="more str"><a href="#">More Details</a></div>
</div>
<div class="clear"></div>
</li>
  	<%
	}
  	%>
	

</ul>
<br/>
  </body>
</html>
