<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add your engine details</title>
<link href="/pages/addEngine/bootstrap.css" rel="stylesheet">

<script language="javascript" type="text/javascript">
var timerid = 0;
var images = new Array(	"/images/image 4.jpg","/images/image 5.jpg","/images/image 6.jpg");
var countimages = 0;
function startTime()
{
	if(timerid)
	{
		timerid = 0;
	}
	var tDate = new Date();
	
	if(countimages == images.length)
	{
		countimages = 0;
	}
	if(tDate.getSeconds() % 5 == 0)
	{
		document.getElementById("image").src = images[countimages];
	}
	countimages++;
	
	timerid = setTimeout("startTime()", 1000);
}
</script>
		

</head>

<body onLoad="startTime();">

<!-- Add engine form -->

<div style="background-color:#EEEEEE;width:400px;float:left;">
<h3>Engine Details</h3>
<form action="/addengine" class="form-add-engine"  role="form" method="post">

  <div class="form-group">
    <label for="">Engine Type</label>
    <input type="Etype" name="eType" class="form-control" id="" placeholder="Enter type">
  </div>
  
  <div class="form-group">
    <label for="">Engine No</label>
    <input type="Etype" name="eNo" class="form-control" id="" placeholder="Enter number">
  </div>
  
  <div class="form-group">
    <label for="">Engine Power</label>
    <input type="Etype" name="ePower" class="form-control" id="" placeholder="">
  </div>
  
  <div class="form-group">
    <label for="">Engine Capacity</label>
    <input type="Etype" name="eCap" class="form-control" id="" placeholder="">
  </div>
  
  <div class="form-group">
    <label for="">Manufacturing Company</label>
    <input type="Etype" name="MC" class="form-control" id="" placeholder="">
  </div>
  
  <div class="form-group">
    <label for="">Engine Usage (Years)</label>
    <input type="Etype" name="eUsage" class="form-control" id="" placeholder="">
  </div>
  
  <div class="form-group">
    <label for="">Price</label>
    <input type="Etype" name="ePrice" class="form-control" id="" placeholder="Rs.">
  </div>
  
  <div class="form-group">
    <label for="">Description About Engine </label>
    <textarea name="eDes" cols="30" rows="4"></textarea>
  </div>
  
  <div class="form-group">
    <label for="exampleInputFile">Upload image</label>
    <input type="file" name="" id="exampleInputFile" accept="image/*">
    </div>
  
  <div class="checkbox">
    <label>
      <input type="checkbox"> I have correctly filled the form
    </label>
  </div>
  

<!-- Indicates a successful or positive action -->
<button type="submit" class="btn btn-success">Submit</button>

<!-- Indicates a successful or positive action -->
<button type="reset" class="btn btn-success">Cancel</button>

  
</form>

</div>
<div style="height:600px;width:100px;float:left;">
</div>


<div style="background-color:#EEEEEE;height:600px;width:600px;float:left;">
<img id ="image" src="/images/image 4.jpg" alt="" class="img-thumbnail" >
</div>
</body>
</html>