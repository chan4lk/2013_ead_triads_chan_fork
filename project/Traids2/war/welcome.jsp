<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A mind blowing way to buy and sell fast cars">
    <meta name="author" content="Triads 2014">   
    <link rel="shortcut icon" href="favicon.ico">

    <title>Welcome to Building Fast Cars</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/welcome.css" rel="stylesheet">
	<link href="css/sticky-footer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <div id="wrap">
 <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/home">Triads Cars</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class=""><a href="/home">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Buy and Sell <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="/buycar">List Car</a></li>
                <li><a href="/urlcheck?act=sell" onclick="">Add Car</a></li>
                <li><a href="/urlcheck?act=order" onclick="">Order Car</a></li>
                <li><a href="/urlcheck?act=engine">Add Engine</a></li>
                <li><a href="/urlcheck?act=list_engine">List Engine</a></li>
                <li><a href="/urlcheck?act=order">order car</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Help</li>
                <li><a href="#">Legal Issues</a></li>                
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><%    UserService userService = UserServiceFactory.getUserService();
    				  User user = userService.getCurrentUser();
				      if (user != null) {
				      pageContext.setAttribute("user", user);
				%>
	
	<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">${fn:escapeXml(user.nickname)} sign out</a>
	<%
	    } else {
	%>
	<a href="/login">Sign in</a>
	<%
    }
	%></li>
            <li><a href="/login">Register</a></li>
            <li class=""><a href="./">Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div class="container" id="contentHost">  	      	
     	<%
     		String url = "/pages/main/main.jsp";
     		if(session.getAttribute("url")!=null)
     		{
     			 url=(String)session.getAttribute("url");    
     		}   
     		pageContext.setAttribute("url", url);
		%>
    	<jsp:include page="${fn:escapeXml(url)}"/>   
    			
    	
    </div><!-- container -->
   	</div><!-- wrap -->
		<div id="footer" class="navbar-fixed-bottom">
	      <div class="container ">
	        <p class="text-muted credit">copyright &copy; <a href="/">Building Fast Cars</a> and <a href="http://chanideals.com/">Chan ideals</a>
	        	  <div id="clock-1" class="clock">
				    <div class="clock-border">
				      <div class="clock-padding">        
				     </div>
				    </div>
				  </div>	       
	      </div>
	    </div>


    <script src="js/jquery.js"></script>   
    <script src="js/bootstrap.min.js"></script>
    <script src="js/clock.js"></script>
	 <script>
	 $('.clock-padding').runningClock({id: 'clock-1-display'})
    	$("input").tooltip();
    	$('.collapse').collapse();
    </script>
  </body>
</html>
