package com.triads.db;

import java.util.ArrayList;         
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.QueryResultList;
import com.triads.model.*;

public class CarHanler {
	public static int addCar(Car car) {
		try {
			Key carKey = KeyFactory.createKey("Car", car.getRegno());
			Entity myCar = new Entity("Car", carKey);

			myCar.setProperty("regno", car.getRegno());
			myCar.setProperty("brand", car.getBrand());
			myCar.setProperty("price", car.getPrice());
			myCar.setProperty("engineno", car.getEngine().getENo());
			myCar.setProperty("eType", car.getEngine().getEtype());
			
			myCar.setProperty("color", car.getColor());
			myCar.setProperty("date", new Date());
			myCar.setProperty("owner", car.getOwnerMail());
			DatastoreService datastore = DatastoreServiceFactory
					.getDatastoreService();
			datastore.put(myCar);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}
	public ArrayList<Car> AllCars() throws EntityNotFoundException {
		ArrayList<Car> cars = new ArrayList<Car>();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try{
		Query q = new Query("Car");		
		
		PreparedQuery pq = ds.prepare(q);
		for (Entity result : pq.asIterable()) {
			Car car = new Car(
					result.getProperty("regno").toString(),
					result.getProperty("brand").toString(),
					result.getProperty("price").toString(),
					result.getProperty("color").toString(),					
					result.getProperty("engineno").toString(),
					result.getProperty("eType").toString()
					);
			cars.add(car);
			}
		
		
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
		}
		return cars;
	}
	public ArrayList<Car> serchCar(String regno) throws EntityNotFoundException {
		ArrayList<Car> cars = new ArrayList<Car>();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try{
		Query q = new Query("Car");
		q.addFilter("regno", FilterOperator.EQUAL,regno);
		
		PreparedQuery pq = ds.prepare(q);
		for (Entity result : pq.asIterable()) {
			Car car = new Car(
					result.getProperty("regno").toString(),
					result.getProperty("brand").toString(),
					result.getProperty("price").toString(),
					result.getProperty("color").toString(),					
					result.getProperty("engineno").toString(),
					result.getProperty("eType").toString()
					);
			cars.add(car);
			}
		return cars;
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public static int  deleteCar(String regno) throws EntityNotFoundException{
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Key key = KeyFactory.createKey("Car", regno);
		ds.delete(key);
		Entity e5=ds.get(key);
		if(key!=e5.getProperty("reg_no")){
			return 0;	
		}
		else {
			return 1;
		}
	}
	
	
	
	
	
}
