package com.triads.db;

import java.util.ArrayList;
import java.util.Date;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.triads.model.Car;
import com.triads.model.Engine;

public class EngineHndler {

	public static int addEngine(Engine engine){
		try{
			Entity myEngine = new Entity("Engine",engine.getENo());
		  
			myEngine.setProperty("eType", engine.getEtype());
			myEngine.setProperty("eNo", engine.getENo());
			myEngine.setProperty("ePower", engine.getEPower());
			myEngine.setProperty("eCap", engine.getECap());
			myEngine.setProperty("MC", engine.getManuCom());
			myEngine.setProperty("eUsage", engine.getEUsage());
			myEngine.setProperty("ePrice", engine.getEPrice());
			myEngine.setProperty("eDes", engine.getEDes());
			myEngine.setProperty("eImage", engine.getEImage());
			myEngine.setProperty("date", new Date());
		    
		    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		    datastore.put(myEngine);
			return 1;
			}
		catch(Exception e)
		{
			e.printStackTrace();
			return -1;
		}
		
		
	}
	
	public ArrayList<Engine> AllEngine() throws EntityNotFoundException {
		ArrayList<Engine> engines = new ArrayList<Engine>();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		try{
		Query q = new Query("Engine");		
		
		PreparedQuery pq = ds.prepare(q);
		for (Entity result : pq.asIterable()) {
			Engine engine = new Engine(
					result.getProperty("eType").toString(),
					result.getProperty("eNo").toString(),
					Float.parseFloat((result.getProperty("ePrice").toString())),
					result.getProperty("ePower").toString(),
					result.getProperty("eCap").toString(),
					result.getProperty("MC").toString(),
					result.getProperty("eUsage").toString(),
					result.getProperty("eDes").toString(),					
					"");
			engines.add(engine);
			}
		
		
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
		}
		return engines;
	}
}
