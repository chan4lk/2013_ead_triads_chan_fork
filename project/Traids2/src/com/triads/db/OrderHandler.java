package com.triads.db;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;

import com.triads.model.Order;;

public class OrderHandler {

		public static int addOrder(Order order) {
			try {
				Entity Order = new Entity("Order", order.getRegno());

				Order.setProperty("regno", order.getRegno());
				Order.setProperty("eno", order.getEno());
				Order.setProperty("date", new Date());

				DatastoreService datastore = DatastoreServiceFactory
						.getDatastoreService();
				datastore.put(Order);
				return 1;
			} catch (Exception e) {
				e.printStackTrace();
				return -1;
			}

		}
		public static int  deleteOrder(String regno) throws EntityNotFoundException{
			DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
			Key key = KeyFactory.createKey("Car", regno);
			ds.delete(key);
			Entity e5=ds.get(key);
			if(key!=e5.getProperty("reg_no")){
				return 0;	
			}
			else {
				return 1;
			}
		}
		
		@SuppressWarnings("unchecked")
		public  static   QueryResultList<Order> getAllOrder(HttpServletRequest req) {
			DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
		    int pageSize = 15;
		    FetchOptions fetchOptions = FetchOptions.Builder.withLimit(pageSize);
		    String startCursor = req.getParameter("cursor");
		    
		    // If this servlet is passed a cursor parameter, let's use it
		    if (startCursor != null) {
		      fetchOptions.startCursor(Cursor.fromWebSafeString(startCursor));
		    }
		    
		    Query q=new Query("Car");
		    PreparedQuery p=ds.prepare(q);
		    QueryResultList<Entity> results = p.asQueryResultList(fetchOptions);
		    results.toArray();
		    
		    
		    //System.out.print(results.get(1).getProperty("userid"));
		   
		   System.out.print(results);  
		   // String []array=(String[]) results.toArray();
		   // ArrayList<Car> a=new ArrayList<Car>();
		    
		   Order order =new Order(results.get(0).toString(),results.get(1).toString());
		    System.out.print(order);
		    return (QueryResultList<Order>) order;
		}
		
		public static int updateOrder(Order order) {
			try {
				Entity Order = new Entity("Order", order.getRegno());

				Order.setProperty("regno", order.getRegno());
				Order.setProperty("eno", order.getEno());
				Order.setProperty("date", new Date());

				DatastoreService datastore = DatastoreServiceFactory
						.getDatastoreService();
				datastore.put(Order);
				return 1;
			} catch (Exception e) {
				e.printStackTrace();
				return -1;
			}

		}
		

		
}
