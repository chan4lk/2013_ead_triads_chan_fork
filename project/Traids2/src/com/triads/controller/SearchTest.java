package com.triads.controller;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;

// Extend HttpServlet class
@SuppressWarnings("serial")
public class SearchTest extends HttpServlet { 
  // Method to handle GET method request.
  @SuppressWarnings("deprecation")
public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
            throws ServletException, IOException
  {
      // Set response content type
      response.setContentType("text/html");	
	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	// Use class Query to assemble a query
	Query q = new Query("Car");
	q.addFilter("regno", FilterOperator.EQUAL, request.getParameter("car-reg-no"));
	// Use PreparedQuery interface to retrieve results
	PreparedQuery pq = datastore.prepare(q);
	String title = "Using GET Method to Read Form Data";
	PrintWriter out = response.getWriter();
	String docType =
			  "<!doctype html public \"-//w3c//dtd html 4.0 " +
			  "transitional//en\">\n" +
	            "<html>\n" +
	            "<head><title>" + title + "</title></head>\n" +
	            "<body bgcolor=\"#f0f0f0\">\n" +
	            "<h1 align=\"center\">" + title + "</h1>\n" ;
	out.println(docType);
	for (Entity result : pq.asIterable()) {
	  String regno = (String) result.getProperty("regno");
	  String brand = (String) result.getProperty("brand");
	  Double price = (Double) result.getProperty("price");			  
	  out.println(
	            "<ul>\n" +
	            "  <li><b>Regno</b>: "
	            + regno + "\n" +
	            "  <li><b>brand</b>: "
	            + brand+ "\n" +
	            "  <li><b>price</b>: "
	            + price+ "\n" 
	            +
	            "</ul>\n" 
	            );
	}
      out.println("</body></html>");
      
  }
  // Method to handle POST method request.
  public void doPost(HttpServletRequest request,
                     HttpServletResponse response)
      throws ServletException, IOException {
     doGet(request, response);
  }
}