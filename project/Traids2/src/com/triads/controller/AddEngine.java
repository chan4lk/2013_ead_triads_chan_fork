package com.triads.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.triads.model.*;
import com.triads.db.*;
@SuppressWarnings("serial")
public class AddEngine extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
			String eType = (String)req.getParameter("eType");
			String eNo = (String)req.getParameter("eNo");
			float ePrice = Float.parseFloat(req.getParameter("ePrice"));
			String ePower = (String)req.getParameter("ePower");
			String eCap = (String)req.getParameter("eCap");
			String MC = (String)req.getParameter("MC");
			String eUsage = (String)req.getParameter("eUsage");
			String eDes = (String)req.getParameter("eDes");
			String eImage = (String)req.getParameter("eImage");
            
			Engine engine = new Engine(eType,eNo,ePrice,ePower,eCap,MC,eUsage,eDes,eImage);
			int sucess = EngineHndler.addEngine(engine);
			if(sucess>0)
				req.getSession().setAttribute("url","/pages/sucess.jsp" );
			resp.sendRedirect("/welcome.jsp");
           
	}
}