package com.triads.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.triads.db.CarHanler;
import com.triads.model.Car;


@SuppressWarnings("serial")
public class SearchCar extends HttpServlet{
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
	// TODO Auto-generated method stub
	
	
	CarHanler carHandler = new CarHanler();
	Car car;
	try {
		String  reg_no=req.getParameter("reg_no");
		resp.getWriter().println(reg_no);
		car = carHandler.serchCar(reg_no).get(0);
		resp.getWriter().println("car brand , " + car.getBrand());
	}
	catch(IllegalArgumentException e)
	{
		resp.getWriter().println("reg_no not provided");
	}
	catch (EntityNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	catch(Exception e)
	{
		resp.getWriter().println(e.getMessage());
	}
   // System.out.println(car);

	
}
}
