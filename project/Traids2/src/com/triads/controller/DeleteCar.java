package com.triads.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.triads.model.Car;

@SuppressWarnings("serial")
public class DeleteCar extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String reg_no = req.getParameter("reg_no");
		Car car = new Car(reg_no);
		System.out.println(car);

		super.doGet(req, resp);
	}
}
