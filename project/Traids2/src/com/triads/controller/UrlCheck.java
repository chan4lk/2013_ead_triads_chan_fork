package com.triads.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class UrlCheck extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {		
		String act = (String) req.getParameter("act");
		String url = null;
		System.out.println("url check");
		if(act.equalsIgnoreCase("sell")){
			System.out.println("sell car");
			url="/pages/addcar/Addcar.jsp";
		}
		else if(act.equalsIgnoreCase("order")){
			url="/pages/ordercar/ordercar.jsp";
		}
		else if(act.equalsIgnoreCase("main")){
			url="/pages/main/main.jsp";
		}
		else if(act.equalsIgnoreCase("engine")){
			System.out.println("add engine");
			url="/pages/addEngine/addEngine.jsp";
		}
		else if(act.equalsIgnoreCase("list_engine")){
			System.out.println("lising engine");
			url="/pages/listEngine/listengine.jsp";
		}
		else
		{
			url="/pages/error.jsp";
		}
		req.getSession().setAttribute("url",url);
		resp.sendRedirect("/"); 
	}
}
