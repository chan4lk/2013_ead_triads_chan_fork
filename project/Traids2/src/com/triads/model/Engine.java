package com.triads.model;

public class Engine {
	private String eType;
	private String eNo;
	private float ePrice ;
	private String ePower;
	private String eCap;
	private String MC;
	private String eUsage;
	private String eImage;
	private String eDes;
	
	public Engine(String eNo)
	{
		this.eNo = eNo;
	}
	
	
	public Engine(String eType, String eNo, float ePrice, String ePower, String eCap, String MC, String eUsage,String eDes,String eImage) {
		
		this.eType = eType;
		this.eNo = eNo;
		this.ePrice = ePrice;
		this.ePower = ePower;
		this.eCap = eCap;
		this.MC = MC;
		this.eUsage = eUsage;
		this.eDes = eDes;
		this.eImage = eImage;
		
	}
	//engine type
	public String getEtype() {
		return eType;
	}
	
	public void setEtype(String eType) {
		this.eType = eType;
	}
	
	//engine no
	public String getENo() {
		return eNo;
	}
	
	public void setENo(String eNo) {
		this.eNo = eNo;
	}
	
	//engine power
	public String getEPower() {
		return ePower;
	}
	
	public void setEPower(String ePower) {
		this.ePower = ePower;
	}
	
	// engine capacity
	public String getECap() {
		return eCap;
	}
	
	public void setECap(String eCap) {
		this.eCap = eCap;
	}
	
	// Manufacturing company
		public String getManuCom() {
			return MC;
		}
		
		public void setManuCom(String MC) {
			this.MC = MC;
		}
	// Engine Usage
		public String getEUsage() {
			return eUsage;
		}
				
		public void setEUsage(String eUsage) {
			this.eUsage = eUsage;
		}	
		// engine price
				public float getEPrice() {
					return ePrice;
				}
						
				public void setEPrice(float ePrice) {
					this.ePrice = ePrice;
				}	
//engine description
	public String getEDes() {
		return eDes;
	}
				
	public void setEDes(String eDes) {
		this.eDes = eDes;
	}
	
	//engine description
		public String getEImage() {
			return eImage;
		}
					
		public void setEImage(String eImage) {
			this.eImage = eImage;
		}
}
	

