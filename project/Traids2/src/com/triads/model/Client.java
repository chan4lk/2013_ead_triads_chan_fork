package com.triads.model;

import java.util.Date;

public class Client {
	
	private String cName;
	private Date cDate;
	private String cMail;
	
	public Client()
	{
		
	}
	public Client(String cMail, String cName, Date cDate)
	{
		this.cMail=cMail;
		this.cName = cName;
		this.cDate = cDate;
	}
	/**
	 * @return the cName
	 */
	public String getcName() {
		return cName;
	}
	/**
	 * @param cName the cName to set
	 */
	public void setcName(String cName) {
		this.cName = cName;
	}
	/**
	 * @return the cDate
	 */
	public Date getcDate() {
		return cDate;
	}
	/**
	 * @param cDate the cDate to set
	 */
	public void setcDate(Date cDate) {
		this.cDate = cDate;
	}
	public String getcMail() {
		return cMail;
	}
	public void setcMail(String cMail) {
		this.cMail = cMail;
	}
	
}
